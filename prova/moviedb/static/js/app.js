var app = angular.module('app', ['ngRoute']);

app.config(['$routeProvider', '$interpolateProvider', function($routeProvider, $interpolateProvider) {
	$interpolateProvider.startSymbol('{!');
    $interpolateProvider.endSymbol('!}');

	$routeProvider.when('/', {
		controller: 'moviesController',
		templateUrl: '/template/p_movie_list'
	})
	.when('/movies/', {
		controller: 'moviesController',
		templateUrl: '/template/p_movie_list'
	})
	.when('/movies/:param', {
		controller: 'moviesController',
		templateUrl: '/template/p_movie_list'
	})
	.when('/genres/', {
		controller: 'genresController',
		templateUrl: '/template/p_genre_list'
	})
	.when('/bydate/', {
		controller: 'datePicker',
		templateUrl: '/template/p_date_pick'
	})
	.when('/movies/:param/:param', {
		controller: 'moviesController',
		templateUrl: '/template/p_movie_list'
	})
	.otherwise({ redirectTo: '/' });
}]);

app.factory('dataFactory', ['$http', function($http){
	var urlBase = '/';
	var dataFactory = {};

	dataFactory.getMovies = function(){
		// console.log(urlBase + 'movies/');
		return $http.get(urlBase + 'movies/');
	};

	dataFactory.getGenres = function(){
		// console.log(urlBase + 'genres/')
		return $http.get(urlBase + 'genres/');
	};

	dataFactory.getMoviesByGenre = function(slug){
		// console.log(urlBase + 'movies/' + slug + '/');
		return $http.get(urlBase + 'movies/' + slug);
	}

	dataFactory.getMoviesByDate = function(year, month){
		// console.log(urlBase + 'movies/' + slug + '/');
		return $http.get(urlBase + 'movies/all/' + year + '/' + month);
	}

	return dataFactory;
}]);

app.controller('genresController', ['$scope', 'dataFactory', '$routeParams', function($scope, dataFactory, $routeParams){
	$scope.param = $routeParams.param;
	$scope.genres;
	$scope.status;

	getGenres();

	function getGenres() {
		dataFactory.getGenres()
		.then(function(response){
			$scope.genres = response.data;
			// console.log($scope.genres);
		}, function(error){
			$scope.status = 'NOT OK. ' + error.message;
		});
	}

	$scope.init = function() {
		$('.collapsible').collapsible({
	      accordion : false
	    });
	}
}]);

app.controller('moviesController', ['$scope', 'dataFactory', '$routeParams', function($scope, dataFactory, $routeParams){
	$scope.param = $routeParams.param; // obtém os parâmetros, se passados
	console.log('$routeparams: ');
	console.log($scope.param);
	$scope.movies;
	$scope.status; // status da resposta (OK, por exemplo, quando dá tudo bom)

	var result = /\w+/.test($scope.param);
	console.log(result);

	if ($scope.param != undefined && /\w+/.test($scope.param)) {
		console.log('obtendo filmes por genero');
		getMoviesByGenre($scope.param);
	}
	if ($scope.param != undefined && /\d{4}-\d{2}/.test($scope.param)) {
		console.log('obtendo filmes por data');
		getMoviesByDate($scope.param.substring(0,4), $scope.param.substring(5,7));
	}
	else getMovies();

	function getMovies() {
		dataFactory.getMovies()
		.then(function(response){
			$scope.movies = response.data;
			// console.log($scope.movies);
		}, function(error){
			$scope.status = 'Não foi possível carregar os filmes [' + error.message + ']';
		});
	}

	function getMovie(id) {
		dataFactory.getMovie(id)
		.then(function(response){
			$scope.movie = response.data;
			// console.log($scope.movie);
		}, function(error){
			$scope.status = 'O filme requisitado não existe. (ou não)'
		});
	}

	function getMoviesByGenre(slug) {
		// console.log('slug: ' + slug);
		dataFactory.getMoviesByGenre(slug)
		.then(function(response){
			$scope.movies = response.data;
			// console.log($scope.movies);
		}, function(error){
			$scope.status = 'Erro ao obter os filmes deste gênero.';
		});
	}

	function getMoviesByDate(year, month) {
		// console.log('date: ' + date);
		dataFactory.getMoviesByDate(year, month)
		.then(function(response){
			$scope.movies = response.data;
			// console.log($scope.movies);
		}, function(error){
			$scope.status = 'Erro ao obter os filmes deste gênero.';
		});
	}

	$scope.init = function() {
		$('.collapsible').collapsible({
	      accordion : false
	    });
	}
}]);

app.controller('datePicker', ['$scope', 'dataFactory', '$routeParams', function($scope, dataFactory, $routeParams){
	$scope.init = function() {
		$('select').material_select();
	}
}]);
from django.contrib import admin
from .models import *

# Register your models here.
# http://localhost:8000/admin
admin.site.register(Movie)
admin.site.register(Genre)
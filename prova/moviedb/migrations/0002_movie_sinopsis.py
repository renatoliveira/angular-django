# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-04-14 00:41
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('moviedb', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='movie',
            name='sinopsis',
            field=models.TextField(default='Não especificado.', max_length=130000),
        ),
    ]

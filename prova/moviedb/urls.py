from django.conf.urls import url
from moviedb import views

urlpatterns = [
	url(r'^app', views.index),
	url(r'^template/(?P<name>(\w{1,}))$', views.template),
	url(r'^genres/$', views.genre_list),
	url(r'^movies/$', views.movie_list),
	url(r'^movies/all/$', views.movie_list),
	url(r'^movies/all/(?P<year>[0-9]{4})/(?P<month>[0-9]{2})/$', views.movies_by_launch_date),
	url(r'^genres/(?P<pk>[0-9]+)/$', views.genre_detail),
	url(r'^movies/(?P<pk>[0-9]+)/$', views.movie_detail),
	url(r'^movies/(?P<slug>\w+)/$', views.movie_by_genre),
	url(r'^movies/(?P<slug>\w+)/(?P<date>(\d{4}-\d{2}))/$', views.movies_by_launch_date_and_genre)
]
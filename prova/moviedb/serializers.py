from rest_framework import serializers
from moviedb.models import Movie, Genre

class MovieSerializer(serializers.ModelSerializer):
	class Meta:
		model = Movie
		fields = ('id', 'title', 'launch_year', 'genre', 'sinopsis')

class GenreSerializer(serializers.ModelSerializer):
	class Meta:
		model = Genre
		fields = ('id', 'slug', 'name')
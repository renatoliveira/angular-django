from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from moviedb.models import *
from moviedb.serializers import *

class JSONResponse(HttpResponse):
	def __init__(self, data, **kwargs):
		content = JSONRenderer().render(data)
		kwargs['content_type'] = 'application/json'
		super(JSONResponse, self).__init__(content, **kwargs)

@api_view(['GET'])
def index(request):
	return render(request,'index.html',{})

@api_view(['GET'])
def template(request, name):
	return render(request, name + '.html', {})

# @csrf_exempt permite que clients que não possuem um token ainda acessem a view
@csrf_exempt
@api_view(['GET'])
def genre_list(request):
	# lista todos os gêneros, ou cria um novo
	if request.method == 'GET':
		genres = Genre.objects.all()
		serializer = GenreSerializer(genres, many=True)
		return JSONResponse(serializer.data)
	elif request.method == 'POST':
		data = JSONParser().parse(request)
		serializer = GenreSerializer(data=data)
		if serializer.is_valid():
			serializer.save()
			return JSONResponse(serializer.data, status=201)
		return JSONResponse(serializer.errors, status=400)

@csrf_exempt
@api_view(['GET', 'PUT', 'DELETE'])
def genre_detail(request, pk):
	try:
		genre = Genre.objects.get(pk=pk)
	except Genre.DoesNotExist:
		return HttpResponse(status=404)

	if request.method == 'GET':
		serializer = GenreSerializer(genre)

		# movies_by_genre = Movie.objects.get(genre=pk)
		# serializer += MovieSerializer(movies_by_genre)

		return JSONResponse(serializer.data)

	elif request.method == 'PUT':
		data = JSONParser().parse(request)
		serializer = GenreSerializer(genre, data=data)
		if serializer.is_valid():
			serializer.save()
			return JSONResponse(serializer.data)
		return JSONResponse(serializer.errors, status=400)

	elif request.method == 'DELETE':
		genre.delete()
		return HttpResponse(status=204)

@csrf_exempt
@api_view(['GET'])
def movie_list(request):
	# lista todos os filmes, ou por gênero
	if request.method == 'GET':
		# print('PATH: ' + str(request.get_full_path))
		# <bound method HttpRequest.get_full_path of <WSGIRequest: GET '/moviedb/movies/'>>
		movies = Movie.objects.all()
		serializer = MovieSerializer(movies, many=True)
		return JSONResponse(serializer.data)
	elif request.method == 'POST':
		data = JSONParser().parse(request)
		serializer = MovieSerializer(data=data)
		if serializer.is_valid():
			serializer.save()
			return JSONResponse(serializer.data, status=201)
		return JSONResponse(serializer.errors, status=400)

@csrf_exempt
@api_view(['GET'])
def movie_by_genre(request, slug):
	# lista todos os filmes, ou por gênero
	if request.method == 'GET':
		# print('PATH: ' + str(request.get_full_path))
		g = Genre.objects.get(slug=slug)
		movies = Movie.objects.filter(genre=g.id)
		serializer = MovieSerializer(movies, many=True)
		return JSONResponse(serializer.data)
	elif request.method == 'POST':
		data = JSONParser().parse(request)
		serializer = MovieSerializer(data=data)
		if serializer.is_valid():
			serializer.save()
			return JSONResponse(serializer.data, status=201)
		return JSONResponse(serializer.errors, status=400)

@csrf_exempt
@api_view(['GET', 'PUT', 'DELETE'])
def movie_detail(request, pk):
	try:
		movie = Movie.objects.get(pk=pk)
	except Movie.DoesNotExist:
		return HttpResponse(status=404)

	if request.method == 'GET':
		serializer = MovieSerializer(movie)
		return JSONResponse(serializer.data)

	elif request.method == 'PUT':
		data = JSONParser().parse(request)
		serializer = MovieSerializer(movie, data=data)
		if serializer.is_valid():
			serializer.save()
			return JSONResponse(serializer.data)
		return JSONResponse(serializer.errors, status=400)

	elif request.method == 'DELETE':
		movie.delete()
		return HttpResponse(status=204)

@api_view(['GET'])
def movies_by_launch_date(request, year, month):
	try:
		movies = Movie.objects.filter(launch_year__year=int(year), launch_year__month=int(month));
	except Movie.DoesNotExist:
		return HttpResponse(status=404)
	serializer = MovieSerializer(movies, many=True)
	return JSONResponse(serializer.data)

@csrf_exempt
@api_view(['GET'])
def movies_by_launch_date_and_genre(request, slug, date):
	try:
		try:
			genre = Genre.objects.get(slug=slug)
		except Genre.DoesNotExist:
			return HttpResponse(status=404)

		movies = Movie.objects.filter(launch_year__year=date[0:4], launch_year__month=date[5:], genre=genre.id)
	except Movie.DoesNotExist:
		return HttpResponse(status=404)

	serializer = MovieSerializer(movies, many=True)
	return JSONResponse(serializer.data)
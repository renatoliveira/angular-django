from django.db import models

# Create your models here.
class Genre(models.Model):
	slug = models.SlugField('slug')
	name = models.CharField('Nome', max_length=255)

	# mostrar o nome do registro ao invés de 'Object Object'
	# na página de admin
	def __str__(self):
		return '[' + str(self.id) + '] ' + self.name

class Movie(models.Model):
	title = models.CharField('Título', max_length=255)
	launch_year = models.DateField('Data de Lançamento')
	genre = models.ForeignKey('Genre')
	sinopsis = models.TextField(default="Não especificado.", max_length=130000)

	# mostrar o nome do registro ao invés de 'Object Object'
	# na página de admin
	def __str__(self):
		return '[' + str(self.id) + '] ' + self.title